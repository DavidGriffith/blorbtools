#!/usr/bin/env perl
# ---------------------------------------------------------------------------
# pix2png: Convert Infocom graphics resource files to individual PNG files.
#	   Also creates chunks for adaptive palette, gamma, release number,
#	     and resolution.
#	   Further processing with infoblorb.pl and pblorb.pl is required
#            to create Blorbs.
#
# (c) David Griffith 2022-2023
#
# Distributed under the terms of the Artistic License 2.0
#
# ---------------------------------------------------------------------------


use strict;
use Digest::CRC qw(crc32);	# libdigest-crc-perl
use IO::Compress::Deflate qw(deflate $DeflateError) ;
use Fcntl 'SEEK_SET';
use Getopt::Long;
use File::Basename;

use constant MAX_BIT => 512;
use constant CODE_SIZE => 8;
use constant CODE_TABLE_SIZE => 4096;

my @adaptive_list;
my @resolution_list;

my $inputfp;
my $input2fp;
my $num;
my %header;

my @directory;
my @colormap;

my @code_table;
my @code_buffer;
my @buffer;

my %comp;

my $screenwidth;
my $screenheight;

my $reln_chunk_name = "reln_0.bin";
my $reso_chunk_name = "reso_0.bin";
my $apal_chunk_name = "apal_0.bin";
my $png_filename = "pict_%03d.png";
my $incomplete = 0;

my $gamename;
my $gamegfx;
my $lastgamename;
my $lastgamegfx;

my $gamma = (0x00, 0x00, 0xd9, 0x04);

my %options;
my $getret;

my @mask = (
	0x0000, 0x0001, 0x0003, 0x0007,
	0x000f, 0x001f, 0x003f, 0x007f,
	0x00ff, 0x01ff, 0x03ff, 0x07ff,
	0x0fff, 0x1fff, 0x3fff, 0x7fff
);

my @ega_colormap = (
	{ red => 0,   green => 0,   blue => 0   },
	{ red => 0,   green => 0,   blue => 170 },
	{ red => 0,   green => 170, blue => 0   },
	{ red => 0,   green => 170, blue => 170 },
	{ red => 170, green => 0,   blue => 0   },
	{ red => 170, green => 0,   blue => 170 },
	{ red => 170, green => 170, blue => 0   },
	{ red => 170, green => 170, blue => 170 },
	{ red => 85,  green => 85,  blue => 85  },
	{ red => 85,  green => 85,  blue => 255 },
	{ red => 85,  green => 255, blue => 85  },
	{ red => 85,  green => 255, blue => 255 },
	{ red => 255, green => 85,  blue => 85  },
	{ red => 255, green => 85,  blue => 255 },
	{ red => 255, green => 255, blue =>  85 },
	{ red => 255, green => 255, blue => 255 },
);


# Entry 0 is not used.  From 1 on, these are the files by number
# which are supposed to have an alpha channel.  The correctness of
# this lookup table is determined from the files found in Kevin Bracey's
# Zork Zero blorb file.
my @zork0_alpha = (
	0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
	0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 
);

# Infocom used 2-bits per color on three files, all from Shogun.
my @shogun_2bit = (3, 44, 59);

$getret = GetOptions('usage|?'	=> \$options{usage},
	'h|help'		=> \$options{help},
	'e|ega'			=> \$options{ega},
	'a|arthur'		=> \$options{arthur},
	'j|journey'		=> \$options{journey},
	's|shogun'		=> \$options{shogun},
	'z|zork0'		=> \$options{zork0}	
	);

my $infile = $ARGV[0];
my $infile2 = $ARGV[1];

if (!$getret || $options{usage} || !$infile) {
	usage();
	exit;
}

if ($options{help}) {
	usage();
	help();
	exit;
}

if ($options{arthur} + $options{journey} + $options{shogun} + $options{zork0} > 1) {
	print "Only one choice of game is allowed.\n";
	exit;
}

my $pass = 1;
while (1) {
	if ($pass == 1) {
		print "\nProcessing $infile.\n";
		open($inputfp, "<", $infile) or die ("Unable to open graphics file $infile");
	} else {
		print "\nProcessing $infile2.\n";
		open($inputfp, "<", $infile2) or die ("Unable to open graphics file $infile2");
	}

	# Check the first pic number to see if we need to swap bytes when reading words.
	# Then rewind back to the beginning.
	seek($inputfp, 16, SEEK_SET);
	$header{reverse} = read_byte($inputfp);
	seek($inputfp, 0, SEEK_SET);

	$header{part} = read_byte($inputfp);
	$header{flags} = read_byte($inputfp);
	$header{unknown1} = read_word($inputfp);
	$header{local_count} = read_word($inputfp);
	$header{global_ptr} = read_word($inputfp);
	$header{entry_size} = read_byte($inputfp);
	$header{padding} = read_byte($inputfp);
	$header{checksum} = read_word($inputfp);
	$header{unknown4} = read_word($inputfp);
	$header{version} = read_word($inputfp);

	print "Total number of images is $header{local_count}\n";
	print "part: $header{part}\n";
	printf("flags: 0x%.2X\n", $header{flags});
	printf("unknown1: %d, 0x%.4X\n", $header{unknown1}, $header{unknown1});
	printf("local_count: %d, 0x%.4X\n", $header{local_count}, $header{local_count});
	printf("global_ptr: %d,  0x%.4X\n", $header{global_ptr}, $header{global_ptr});
	printf("entry_size: %d,  0x%.4X\n", $header{entry_size}, $header{entry_size});
	printf("padding: %d, 0x%.4X\n", $header{padding}, $header{padding});
	printf("checksum: %d, 0x%.4X\n", $header{checksum}, $header{checksum});
	printf("unknown2: %d, 0x%.4X\n", $header{unknown2}, $header{unknown2});
	printf("version: %d, 0x%.4X\n", $header{version}, $header{version});
	printf("reverse: %d, 0x%.4X\n", $header{reverse}, $header{reverse});
	game_guess($header{local_count}, $pass);
	if ($pass == 1) {
		$lastgamename = $gamename;
		$lastgamegfx = $gamegfx;
	} else {
		if ( ($lastgamename ne $gamename) || ($lastgamegfx ne $gamegfx)) {
			print "Second file does not go with the first.  Try again.\n";
			exit 1;
		}
	}

	for (my $i = 0; $i < $header{local_count}; $i++) {
		$directory[$i]{image_number} = read_word($inputfp);
		$directory[$i]{image_width} = read_word($inputfp);
		$directory[$i]{image_height} = read_word($inputfp);
		$directory[$i]{image_flags} = read_word($inputfp);
		$directory[$i]{image_data_addr} = read_byte($inputfp) << 16;
		$directory[$i]{image_data_addr} += read_byte($inputfp) << 8;
		$directory[$i]{image_data_addr} += read_byte($inputfp);

		if ($header{entry_size} == 14) {
			$directory[$i]{image_cm_addr} = read_byte($inputfp) << 16;
			$directory[$i]{image_cm_addr} += read_byte($inputfp) << 8;
			$directory[$i]{image_cm_addr} += read_byte($inputfp);
		} else {
			$directory[$i]{image_cm_addr} = 0;
			read_byte($inputfp);
		}
	}

	for (my $i = 0; $i < $header{local_count}; $i++) {
		process_image($inputfp, $directory[$i]);
	}
	close($inputfp);

	if ($pass == 2 || !$infile2) {
		last;
	}
	$pass++;
}



if ($header{version}) {
	print "Writing picture release number chunk.\n";
	write_reln($header{version});
}

if (@adaptive_list) {
	print "Writing adaptive palette chunk.\n";
	write_apal(@adaptive_list);
}

if (@resolution_list) {
	print "Writing resolution chunk.\n";
	if ($screenwidth && $screenheight) {
		write_reso($screenwidth, $screenheight, @resolution_list);
	} else {
		write_reso(320, 200, @resolution_list);
	}
}

if ($incomplete) {
	print "Re-run this script on both files at once.\n";
} else {
	print "All files accounted for.\n";
}

exit 0;

# ---------------------------------------------------------------------------

sub usage
{
	my $name = basename($0);
	print "Usage: $name [-a|-h|-j|-s|-z] $0 <graphicsfile> [<graphicsfile2>]\n";
}

sub help
{
	print "Help text goes here.\n\n";
}

sub game_guess
{
	my ($image_count, $pass, @junk) = @_;

	my $specified = 0;
	my $inferred = 0;


	# If one of these is deliberately set at the command line...
	if ($options{arthur} || $options{journey} ||
	  $options{shogun} || $options{zork0}) {
		$specified = 1;
	} else {		# Otherwise we infer what the game is
		$inferred = 1;	#   based on image count.
	}

	# For the second file, infer its identify.
	# Then check to see if it really goes with the first.
	if ($pass > 1) {
		$options{arthur} = $options{journey} = $options{shogun} = $options{zork0} = 0;
	}
	print "game name: ";

	# Inferences based on image count.
	if ($options{arthur} || $image_count == 171 || $image_count == 125 || $image_count == 101) {
		print "Arthur ";
		if ($image_count == 125) {
			$incomplete = 1;
			$options{ega} = 1;
			print "(Part 1) ";
		}
		if ($image_count == 101) {
			$incomplete = 0;
			$options{ega} = 1;
			print "(Part 2) ";
		}
		$options{arthur} = 1;
		$gamename = "arthur";
	}
	if ($options{journey} || $image_count == 134) {
		print "Journey ";
		$gamename = "journey";
		$options{journey} = 1;
	}
	if ($options{shogun} || $image_count == 48 || $image_count == 50) {
		print "Shogun ";
		if ($image_count == 50) {
			$options{ega} = 1;
		}
		$gamename = "shogun";
		$options{shogun} = 1;
	}
	if ($options{zork0} || $image_count == 503) {
		print "Zork Zero ";
		$gamename = "zork0";
		$options{zork0} = 1;
	}

	# If none of those image count tests worked,
	#   I don't know what game this is.
	if (0 == ($options{arthur} || $options{journey} || $options{shogun} || $options{zork0})) {
		print "unknown or unrecognized.\n";
		$gamename = "unknown";
		$inferred = 0;		# Just in case.
		return;
	}

	if ($inferred) {
		print "(inferred)\n";
		$specified = 0;		# Just in case.
	}

	if ($specified) {
		print "(specified)\n";
	}

	print "graphics type: ";
	if ($options{ega}) {
		$gamegfx = "ega";
		print "EGA\n";
		if ($options{shogun} || $options{zork0}) {
			if ($pass == 2) {
				goto NOPASS;
			}
		}
		return;
	}
	print "MCGA/VGA\n";
	if ($pass == 2) {
	}

NOPASS:
	print "No need for a second graphics file for this game and graphics combination.\n";
	return;
}

sub read_byte
{
	my ($fp, @junk) = @_;
	my $retval;
	read($fp, $retval, 1);
	return unpack("C", $retval);
}

sub read_word
{
	my ($fp, @junk) = @_;
	my $retval;
	read($fp, $retval, 2);
	return unpack("S", $retval);
}

sub read_long
{
	my ($fp, @junk) = @_;
	my $retval;
	read($fp, $retval, 4);
	return unpack("L", $retval);
}

sub write_reln
{
	my ($release_number, @junk) = @_;
	my $fp;
	open($fp, ">", $reln_chunk_name) or die ("Unable to write to $reln_chunk_name");
	print $fp pack("n", $release_number);
	close($fp);
	return;
}

sub write_apal
{
	my @list = @_;
	my $fp;
	my @temp;
	my $image_number;

	open($fp, ">", $apal_chunk_name) or die ("Unable to write to $apal_chunk_name");
	while (@list) {
		$image_number = pop(@list);
		push(@temp, $image_number);
	}

	while (@temp) {
		print $fp pack("N", pop(@temp));
	}
	close($fp);
	return;
}

sub write_reso
{
	my ($width, $height, @list) = @_;
	my $fp;
	my @temp;
	my $image_number;

	open($fp, ">", $reso_chunk_name) or die ("Unable to write to $reso_chunk_name");

	while (@list) {
		$image_number = pop(@list);
		# Split .eg1 and .eg2 files will result in duplicates
		#  which are weeded out here.
		if (!grep(/^$image_number$/, @temp)) {
			push(@temp, $image_number);
		}
	}

	# Standard window width
	print $fp pack("N", $width);

	# Standard window height
	print $fp pack("N", $height);

	# Minimum window width
	print $fp pack("N", 0);

	# Minimum window height
	print $fp pack("N", 0);

	# Maximum window width
	print $fp pack("N", 0);
	
	# Maximum window height
	print $fp pack("N", 0);

	while (@temp) {
		# Image resource number
		print $fp pack("N", pop @temp);

		# Numerator of standard ratio
		print $fp pack("N", 1);
	
		# Denominator of standard ratio
		print $fp pack("N", 1);

		# Numerators and denominators of minimum and maximum ratios
		print $fp pack("N", 0);
		print $fp pack("N", 0);
		print $fp pack("N", 0);
		print $fp pack("N", 0);
	}
	close($fp);
	return;
}  # write_reso

sub write_rectangle
{
	my $picnum = $_[0];
	my $fp;
	my %image = %{$_[1]};
	my $rect_filename = sprintf("pict_%03d.rec", $picnum);
	open($fp, ">", $rect_filename) or die ("Unable to write $rect_filename");
	print $fp pack("N", $image{width});
	print $fp pack("N", $image{height});
	close($fp);
	return;
}

sub write_chunk
{
	my $fp = $_[0];
	my $data = $_[1];

	my $len=length($data)-4;
	my $crc = Digest::CRC->new(type=>"crc32");
	$crc->add($data);
	print $fp pack('N',$len),$data,pack('N',$crc->digest);
}

sub process_image
{
	my $fp = $_[0];
	my %dir = %{$_[1]};
	my $colors = 16;
	my $color_read;
	my %image;

	# Initialize colormap with the EGA colormap as default.
	for (my $i = 0; $i < 16; $i++) {
		$colormap[$i]{red}   = $ega_colormap[$i]{red};
		$colormap[$i]{green} = $ega_colormap[$i]{green};
		$colormap[$i]{blue}  = $ega_colormap[$i]{blue};
	}

	if ($dir{image_cm_addr}) {
		if (!seek($fp, $dir{image_cm_addr}, SEEK_SET)) {
			die "Unable to seek $infile to $dir{image_cm_addr}";
		}
		$colors = read_byte($fp);

		# Fix for some buggy Arthur pictures.
		if ($colors > 14) {
			$colors = 14;
		}
		$color_read = $colors * 3;
		$colors += 2;
		for (my $i = 2; $i < $colors; $i++) {
			$colormap[$i]{red} = read_byte($fp);
			$colormap[$i]{green} = read_byte($fp);
			$colormap[$i]{blue} = read_byte($fp);
		}
	}

	if ($dir{image_flags} & 1) {
		$colormap[$dir{image_flags} >> 12]{red} = 0;
		$colormap[$dir{image_flags} >> 12]{green} = 0;
		$colormap[$dir{image_flags} >> 12]{blue} = 0;
	}

	$image{monochrome} = 0;
	# Fix for CGA images that should be simply black and white.
	if ($dir{image_flags} & 0x08){
		$image{monochrome} = 1;
		$colormap[2]{red} = 255;
		$colormap[2]{green} = 255;
		$colormap[2]{blue} = 255;
		$colormap[3]{red} = 0;
		$colormap[3]{green} = 0;
		$colormap[3]{blue} = 0;
	}

	if ($dir{image_data_addr} == 0) {
		$colors = 0;
	}

	printf("pic %03d   size %3d x %3d   %2d colours   colour map ",
		$dir{image_number},
		$dir{image_width},
		$dir{image_height},
		$colors);

	if ($dir{image_cm_addr} != 0) {
		printf("0x%05lx", $dir{image_cm_addr});
	} else {
		print "-------";
	}

	# If processing for Zork Zero, use our lookup table @zork0_alpha.
	if ($dir{image_flags} & 1) {
		if ($options{zork0} && !$zork0_alpha[$dir{image_number}]) {
			$image{transflag} = 0;
			$image{transpixel}= 0;
			print "\n";
		} else {
			$image{transflag} = 1;
			$image{transpixel} = $dir{image_flags} >> 12;
			printf("   transparent is %u\n", $image{transpixel});
		}
	} else {
		$image{transpixel} = 0;
		$image{transflag} = 0;
		print "\n";
	}

	$image{width} = $dir{image_width};
	$image{height} = $dir{image_height};

	push(@resolution_list, $dir{image_number});

	# Work out correct screen resolution.
	if (($options{zork0} || $options{shogun}) && ($dir{image_number} == 1)) {
		$screenwidth = $image{width};
		$screenheight = $image{height};
	}

	if ($options{arthur} && $dir{image_number} == 1) {
		if ($image{width} == 584) {
			$screenwidth = 640;
			$screenheight = 200;
		}
	}

	if ($dir{image_data_addr} == 0) {
		write_rectangle($dir{image_number}, \%image);
		return;
	}

	if ($dir{image_cm_addr} == 0) {
		push(@adaptive_list, $dir{image_number});
	}

	$image{colors} = $colors;
	$image{pixels} = 0;

	$image{colormap} = @colormap;

	if (!seek($fp, $dir{image_data_addr}, SEEK_SET)) {
		die "Unable to seek $infile to $dir{image_number}";
	}

	%image = decompress_image($fp, \%image);
	write_png_file($dir{image_number}, \%image);

	return;
}  # process_image

sub decompress_image
{
	my $fp = $_[0];
	my %image = %{$_[1]};

	my $i;
	my $code;
	my $old;
	my $first;
	my $clear_code;

	$clear_code = 1 << CODE_SIZE;
	$comp{next_code} = $clear_code + 2;
	$comp{slen} = 0;
	$comp{sptr} = 0;
	$comp{tlen} = CODE_SIZE + 1;
	$comp{tptr} = 0;

	for ($i = 0; $i < CODE_TABLE_SIZE; $i++) {
		$code_table[$i]{prefix} = CODE_TABLE_SIZE;
		$code_table[$i]{pixel} = $i;
	}

	while (1) {
		if (($code = read_code($fp)) == ($clear_code + 1)) {
			return %image;
		}

		if ($code == $clear_code) {
			$comp{tlen} = CODE_SIZE + 1;
			$comp{next_code} = $clear_code + 2;
			$code = read_code($fp);
		} else {
			$first = ($code == $comp{next_code}) ? $old : $code;
			while ($code_table[$first]{prefix} != CODE_TABLE_SIZE) {
				$first = $code_table[$first]{prefix};
			}
			$code_table[$comp{next_code}]{prefix} = $old;
			$code_table[$comp{next_code}++]{pixel} = $code_table[$first]{pixel};
		}
		$old = $code;
		$i = 0;
		do {
			$buffer[$i++] = $code_table[$code]{pixel};
		} while (($code = $code_table[$code]{prefix}) != CODE_TABLE_SIZE);

		do {
			$image{image}[$image{pixels}++] = $buffer[--$i];
		} while ($i > 0);
	}
	# This should not be reached.
	die "You should not be here";
}  # decompress_image


sub write_png_file
{
	my $image_number = $_[0];
	my %image = %{$_[1]};

	my $fp;
	my $filename = sprintf($png_filename, $image_number);

	my $width = $image{width};
	my $height = $image{height};

	my $IHDR;
	my $gAMA;
	my $PLTE;

	open($fp, ">", $filename) or die ("Unable to write to $filename");

	# Write PNG header chunk.
	print $fp "\x89PNG\x0d\x0a\x1a\x0a";

	# Prepare PNG IHDR chunk
	$IHDR = 'IHDR';
	$IHDR .= pack('N', $width);	# width
	$IHDR .= pack('N', $height);	# height
	$IHDR .= pack('C', 4);		# bit depth
	$IHDR .= pack('C', 3);		# color type
	$IHDR .= pack('C', 0);		# compression type
	$IHDR .= pack('C', 0);		# filter type
	$IHDR .= pack('C', 0);		# interlace type
	&write_chunk($fp, $IHDR);

	# Prepare PNG gAMA (gamma) chunk
	$gAMA = 'gAMA';
	$gAMA .= pack("C", 0);
	$gAMA .= pack("C", 0);
	$gAMA .= pack("C", 0xd9);
	$gAMA .= pack("C", 0x04);
	&write_chunk($fp, $gAMA);

	# Prepare PNG PLTE (palette) chunk
	$PLTE = 'PLTE';
	for (my $i = 0; $i < $image{colors}; $i++) {
		$PLTE .= pack('C', $colormap[$i]{red});
		$PLTE .= pack('C', $colormap[$i]{green});
		$PLTE .= pack('C', $colormap[$i]{blue});
	}
	&write_chunk($fp, $PLTE);

	if ($image{transflag}) {
		my $tRNS = "tRNS" . "\000";
		&write_chunk($fp, $tRNS);
	}

	# Prepare PNG IDAT chunk
	my $output;
	my @data;

	my $data_ref = @image{image};
	@data = @$data_ref;

#	my $raw_filename = "pict_%03d.raw";
#	my $raw_fp;
#	my $raw_file = sprintf($raw_filename, $image_number);
#	open($raw_fp, ">", $raw_file) or die ("Unable to write to $raw_file");
#	print $raw_fp $output;
#	close $raw_fp;

	if (!$options{ega} && $options{shogun} && grep(/^$image_number$/, @shogun_2bit)) {
		@data = pack_halfnibbles($image{width}, @data);
	} else {
		@data = pack_nibbles($image{width}, @data);
	}

	$output = pack("C*", @data);


	my $deflated;
	my $status = deflate \$output => \$deflated
		or die "deflate failed: $DeflateError\n";
	my $IDAT="IDAT" . $deflated;

	write_chunk($fp, $IDAT);
	write_chunk($fp, 'IEND');

	close $fp;
	return;

}  # write_png_file

sub pack_halfnibbles
{
	my ($width, @data) = @_;

	my @packed_data;
	my @bits;
	my $pixels = scalar @data;
	my $lines = $pixels / $width;

	my $left_nibble1;
	my $left_nibble2;
	my $left_nibble;

	my $right_nibble1;
	my $right_nibble2;
	my $right_nibble;

	my $eol = 0;

	my $linecount = 0;

	my $remaining = $width;

	push @packed_data, 0;
	while (@data) {
		if ($remaining == 3) {
			$left_nibble1 = (shift @data) << 6;
			$left_nibble2 = (shift @data) << 4;
			$left_nibble = $left_nibble1 + $left_nibble2;
			$right_nibble = (shift @data) << 2;
			$eol = 1;
		}
		if ($remaining == 2) {
			$left_nibble1 = (shift @data) << 6;
			$left_nibble2 = (shift @data) << 4;
			$left_nibble = $left_nibble1 + $left_nibble2;
			$right_nibble = 0;
			$eol = 1;
		}
		if ($remaining == 1) {
			$left_nibble = (shift @data) << 6;
			$right_nibble = 0;
			$eol = 1;
		}
		if ($remaining >= 4) {
			$left_nibble1 = (shift @data) << 6;
			$left_nibble2 = (shift @data) << 4;
			$left_nibble = $left_nibble1 + $left_nibble2;
			$right_nibble1 = (shift @data) << 2;
			$right_nibble2 = (shift @data);
			$right_nibble = $right_nibble1 + $right_nibble2;
			$remaining -= 4;
		}

		my $value = $left_nibble + $right_nibble;
		push @packed_data, $value;

		if ($eol || $remaining == 0) {
			$eol = 0;
			$remaining = $width;
			push @packed_data, 0;
		}
	}
	pop @packed_data;
	return @packed_data;
}

sub pack_nibbles
{
	my ($width, @data) = @_;
	my @output;
	my $test;
	my $count = 0;
	my $line = 0;
	my $return_scalar;

	my $need_shift = 1;
	my $temp;
	
#	push @output, 0;
	foreach my $i (@data) {
		if ($width > 0 && ($count >= $width)) {
			$temp = pop @output;
			push @output, $temp;
			push @output, 0;
			push @output, $i << 4;
			$need_shift = 0;
			$count = 1;
			$line++;
			next;
		}
		if ( ($count == 0) && !$need_shift) {
			$temp = pop @output;
			push @output, $temp + $i;
			$need_shift = 1;
			$count++;
			next;
		}
		if (!$need_shift) {
			$test = pop @output;
			$count++;
			$need_shift = 1;
			push @output, $i + $test;
			next;
		}
		if ($need_shift) {
			$temp = pop @output;
			push @output, $temp;
			push @output, ($i << 4);
			$count++;
			$need_shift = 0;
			next;
		}
	}
	return @output;
}

sub read_code
{
	my $fp = $_[0];

	my $code;
	my $bsize;
	my $tlen = $comp{tlen};
	my $tptr;

	my $i = 0;

	$code = 0;
	$tptr = 0;
	$tptr = 0;

	while ($tlen) {
		if ($comp{slen} == 0) {
			for ($i = 0; $i < MAX_BIT; $i++) {
				read($fp, $code_buffer[$i], 1);
				$code_buffer[$i] = unpack("C", $code_buffer[$i]);
			}
			$comp{slen} = $i;
			if ($comp{slen} == 0) {
				die "Problem reading";
			}
			$comp{slen} *= 8;
			$comp{sptr} = 0;
		}
		$bsize = (($comp{sptr} + 8) & ~7) - $comp{sptr};
		$bsize = ($tlen > $bsize) ? $bsize : $tlen;
		$code |= (($code_buffer[$comp{sptr} >> 3] >> ($comp{sptr} & 7)) & 
			$mask[$bsize]) << $tptr;

		my $fee = $code_buffer[$comp{sptr} >> 3];
		my $fie = $comp{sptr} & 7;
		my $foe = $mask[$bsize];
		my $fum = $mask[$bsize] << $tptr;

		$tlen -= $bsize;
		$tptr += $bsize;
		$comp{slen} -= $bsize;
		$comp{sptr} += $bsize;
	}

	if (($comp{next_code} == $mask[$comp{tlen}]) && ($comp{tlen} < 12)) {
		$comp{tlen}++;
	}
	return $code;
}  # read_code
