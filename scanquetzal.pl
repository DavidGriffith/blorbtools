#!/usr/bin/perl -w
# ---------------------------------------------------------------------------
#  scanQuetzal: a perl script for scanning Quetzal files
#  (c) David Griffith 2020-2023
#
#  Distributed under the terms of the Artistic License 2.0
#
#  Based on work by Graham Nelson
#
# ---------------------------------------------------------------------------


use strict;
use Getopt::Long;
use Pod::Usage;
use Encode qw(decode);

require 5.6.0;

my $name = "scanquetzal";
my $version = "0.1";
my $tagline = "$name $version";

my $buffer;
my %options;
my %images;
my %sounds;
my %execs;

my $imagecount;
my $execcount;
my $soundcount;

my $xmlcount = 0;
my $resocount = 0;
my $apalcount = 0;
my $loopcount = 0;

GetOptions('usage|?'	=> \$options{usage},
	'h|help'	=> \$options{help},
	'e|extract'	=> \$options{extract},
	'v|version'	=> \$options{version}
	);

if ($options{version}) {
	print "$tagline\n";
	exit;
}

my $input_filename = $ARGV[0];
my $output_filename;

pod2usage(1) if $options{usage};
pod2usage(-verbose => 3) if $options{help};
pod2usage(1) if !$input_filename;


sub array_diff(\@\@);

my ($sec,$min,$hour,$mday,$month,$year) = (localtime(time))[0, 1, 2, 3, 4, 5];

my $blorbdate = sprintf("%04d/%02d/%02d at %02d:%02d.%02d",
                 $year + 1900, $month + 1, $mday, $hour, $min, $sec);


print STDOUT "$tagline [executing on $blorbdate]\n";
print STDOUT "Filename: $input_filename.\n";

open (BLORB, $input_filename) or die "Can't load $input_filename.";
binmode(BLORB);

read BLORB, $buffer, 12;

my ($junk1, $length, $junk2) = unpack("NNN", $buffer);

my $file_length = -s $input_filename;
my $groupid = substr($buffer, 0, 4);
my $type = substr($buffer, 8, 4);

$groupid eq "FORM" or die "Not a valid FORM file!\n";
$type eq "IFZS" or die "Not a Quetzal file!\n";

print "File length: $file_length bytes\n";
print "Form length: $length bytes\n";

my ($size, $pos);


# These chunks may appear within an IFZS FORM:
#	IFhd	Game identifier
#	CMem	Compressed data
#	UMem	Uncompressed data
#	Stks	Stack
#	IntD	Interpreter-dependent information

for($pos = 12; $pos < $length; $pos += $size + ($size % 2) + 8) {
	my $chunkdata;

	read(BLORB, $buffer, 8) == 8
		or die("Incomplete chunk header at $pos\n");

	$size = (unpack("NN", $buffer))[1]; # second word of header
	my $type = substr($buffer, 0, 4);
	printf "%06x: $type chunk with $size bytes of data\n", $pos;

	read(BLORB, $chunkdata, $size) == $size
		or die("Incomplete chunk at $pos\n");

	if (!$chunkdata) { next; }

	if($size % 2) { read(BLORB, $buffer, 1); }

	# optional chunks
	if ($type eq "(c) ") {
		print "\tCopyright: $chunkdata\n";
	}
	if ($type eq "AUTH") {
		print "\tAuthor: $chunkdata\n";
	}
	if ($type eq "ANNO") {
		print "\tAnnotations: $chunkdata\n";
	}

	if ($type eq "IFhd") {
		my $release = unpack("n", substr($chunkdata,0, 2));
		my $serialcode = substr($chunkdata, 2, 6);
		my $checksum = unpack("n", substr($chunkdata, 8, 2));
		my @pc_part = unpack("ccc", substr($chunkdata, 10, 3));
		my $pc = ($pc_part[0] << 16) + ($pc_part[1] << 8) + $pc_part[2];

		print "\tRelease: $release  ";
		print "Serial number: $serialcode  ";
		printf "Checksum: %X  ", $checksum;
		printf "PC: %X", $pc;
		print "\n";

		dumpchunk("ifhd.bin", $pos, $chunkdata);
	}

	if ($type eq "CMem") {
		dumpchunk("cmem.bin", $pos, $chunkdata);
	}

	if ($type eq "UMem") {
		dumpchunk("umem.bin", $pos, $chunkdata);
	}

	if ($type eq "Stks") {
		dumpchunk("stks.bin", $pos, $chunkdata);
	}
}

close(BLORB);

# ---------------------------------------------------------------------------

sub array_diff(\@\@) {
	my %e = map { $_ => undef } @{$_[1]};
	return @{[ ( grep { (exists $e{$_}) ? ( delete $e{$_} ) : ( 1 ) } @{ $_[0] } ), keys %e ] };
}

sub warn_resource {
	my ($pos, @junk) = @_;
	my $errstr = sprintf("No resource information for chunk at %0x06x\n", $pos);
	warn($errstr);
}

sub dumpchunk {
	my ($filename, $pos, $chunkdata, @junk) = @_;

	if (!$options{extract}) {
		return;
	}

	open CHUNKFH, ">$filename"
		or warn "Failed to open handle for $filename: $!\n", next;
	binmode CHUNKFH;
	$\ = undef;
	print CHUNKFH $chunkdata;
	close CHUNKFH
		or warn "Failed to close handle for $filename: $!\n", next;
}

# ---------------------------------------------------------------------------

