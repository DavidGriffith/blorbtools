#!/usr/bin/env perl
# ---------------------------------------------------------------------------
#  infoblorb: a preprocessor for producing blorb file from infocom graphics
#  (c) David Griffith 2021-2023
#
#  First the *.cg1, *.eg?, or *.mg1 file is opened with pix2gif in ztools.
#  This extracts all the images in .gif format and produces some helper
#  chunks.  These helper chunks are rectangles, release number, resolution,
#  and adaptive palette.  Rectangles simply describe the dimensions of a
#  a blank rectangle.
#
#  Distributed under the terms of the Artistic License 2.0
#
#  Latest version is at https://gitlab.com/DavidGriffith/blorbtools
# ---------------------------------------------------------------------------

use strict;
use Getopt::Long;	# libsort-fields-perl in Debian
use Sort::Fields;

my $file_sep    = '/';		# Character used to separate directories in
				# pathnames (on most systems this will be /)

my $output_filename = 'output.blurb';
my $infile;
my $num;

my $reln_line;
my $apal_line;
my $reso_line;

my $rect_height;
my $rect_width;

my @stack;

my %options;
my $getret;

$getret = GetOptions('usage|?'	=> \$options{usage},
	'h|help'		=> \$options{help},
	'o|output=s'		=> \$options{output}
	);

if ($options{output}) {
	$output_filename = $options{output};
}

# ---------------------------------------------------------------------------


foreach my $i (@ARGV) {
	if ($i =~ /^.*[Pp][Nn][Gg]$/) {
		$num = $i;
		$num =~ s/\D//g;
		push @stack, "picture $num \"$i\" scale min 0/0 max 0/0\n";	
	}
	elsif ($i =~ /^.*[Rr][Ee][Cc]$/) {
		open(INFILE, "<$i") or die "unable to read $i";
		read(INFILE, $rect_width, 4);
		read(INFILE, $rect_height, 4);
		$num = $i;
		$num =~ s/\D//g;
		$rect_width = unpack("N", $rect_width);
		$rect_height = unpack("N", $rect_height);
		push @stack, "rectangle $num $rect_width $rect_height\n";
		close(INFILE);
	}
	elsif ($i =~ /^apal.*/) {
		$apal_line = "adaptive_palette \"$i\"\n";
	}
	elsif ($i =~ /^reso.*/) {
		$reso_line = "resolution_chunk \"$i\"\n";
	}
	elsif ($i =~ /^reln.*/) {
		open(INFILE, "<$i") or die "unable to read $i";
		if (2 != read(INFILE, $num, 2)) { die "unable to read two bytes from $i"; }
		$reln_line = "release " . unpack("n", $num) . "\n";
		close(INFILE);
	}
}

sort @stack;

open (OUTFILE, ">$output_filename") or die "Unable to write $output_filename";

print OUTFILE "! Blorb file for GAMENAME graphics\n";
print OUTFILE "! Created with infoblorb.pl by David Griffith (c) 2021.\n";
print OUTFILE "\n";
print OUTFILE "! Edit these lines for the particular game whos resources you are processing.\n";
print OUTFILE "! copyright \"Infocom 1988\"\n";
print OUTFILE "! resolution 320x200\n";
print OUTFILE "! storyfile \"zork0.z6\" include\n";
print OUTFILE "\n";
print OUTFILE "! Special chunks.\n";
print OUTFILE $reln_line;
print OUTFILE $apal_line;
print OUTFILE $reso_line;

# Sort all picture and rectangle lines by resource number.
@stack = fieldsort ['2n'], @stack;

print OUTFILE "\n! Picture / Rectangle chunks.\n";
foreach my $i (@stack) {
	print OUTFILE $i;
}
