#!/usr/bin/env perl
# ---------------------------------------------------------------------------
# infoalpha: Examine a list of image files and determines if each has an 
#	     alpha channel or not.  Then emit an array of bits indicating
#	     as such.  The index corresponds to the file number.
#	     This program's primary purpose is to assist in examining
#	     Kevin Bracey's work with Blorb files.
#
# (c) David Griffith 2022-2023
#
# Distributed under the terms of the Artistic License 2.0
#
# ---------------------------------------------------------------------------


my $result;
my @alpha_list;
my @alpha_lookup;
my $num;
my $dummy;

my $filecount = scalar @ARGV;


print "These files have an alpha channel: ";
foreach my $file (@ARGV) {
	if (!`file $file | grep PNG`) {
		push @alpha_list, $num;
		next;
	}

	$result = `identify -verbose $file | grep -e "Filename:" -e "Alpha: 1-bit" | sed 's/^[ \t]*//' `;
	if ($result =~ /Alpha/) {
		$num = $file;
		$num =~ /Filename/;
		($dummy, $num) = split("_", $num);
		$num =~ s/\.[^.]+$//;
		$num =~ s/^[0]*//;
		push @alpha_list, $num;
		print "$num ";	
	}
}

push @alpha_lookup, 0;
for (my $i = 1; $i < $filecount; $i++) {
	if (grep(/^$i$/, @alpha_list)) {
		push @alpha_lookup, 1;
	} else {
		push @alpha_lookup, 0;
	}
}

print "\n\nNow here's a lookup table to add to pix2png.pl:\n\n";

print "my \@zork0_alpha = (\n";
my $count = 0;
my $width = 20;
foreach my $i (@alpha_lookup) {
	if ($count == 0) {
		print "\t$i, ";
		$count++;
		next;
	} else {
		print $i;
		if ($count >= $width) {
			print ",\n";
			$count = 0;
		} else {
			print ", ";
			$count++;
		}
	}
}
print "\n);\n";


#print "\nNumber of non-alpha-channel files: " . scalar @noalpha_list . "\n";

## $value can be any regex. be safe
#if ( grep( /^$value$/, @array ) ) {
#  print "found it";
#}



